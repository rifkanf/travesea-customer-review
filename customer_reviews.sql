-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2017 at 09:31 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_reviews`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `commenter` varchar(255) NOT NULL,
  `time_comment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `review_id`, `comment`, `commenter`, `time_comment`) VALUES
(1, 1, 'setuju banget mba', 'Kev''s Mommy', '2017-02-02 11:24:08'),
(3, 1, 'Wah kaykna asik ya', 'New Mommy', '2017-02-02 17:14:49'),
(4, 3, 'Sangat berfaedah', 'AnakLaut', '2017-02-02 17:17:15'),
(5, 3, 'Indah sekali', 'SukaJalan', '2017-02-02 17:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `review_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`review_id`, `title`, `content`, `author`, `time`) VALUES
(1, 'Jalan - Jalan Lihat Ikan', 'Okeh. Ceritanya sabtu kemarin dari pagi-pagi buta saya sama airen udah heboh jumpalitan bangun, mandi, siap-siap karena saya bakal mengikuti ujian jam 7 pagi di depok. Begitu sampe lokasi, kok sepiiiiii banget yah. Lantas saya mengecek kartu ujian. TETOOOT!!! Ternyata... ujiannya besok sodara sodara!!! saya pikir tanggal 13 itu sabtu, tau-nya hari minggu... hadeeehhh kebodohan. Pulang lagi deh kita.\r\n\r\nDi tengah jalan, tiba-tiba ibu saya menelepon, ngasih kabar kalau mau ngajak pergi Gendhis ke Seaworld. Woooowww... emang udah cita-cita saya nih buat ngajak adek kesana. Awalnya sempet gamang, niat nya hari ini mau ngendon di rumah aja karena masih punya waktu extra buat belajar. Tapi kok.... saya juga excited penasaran pengen liat gimana reaksi Gendhis kalo nontonin ikan buanyaaak di aquarium yang super gede. Pasti dia bakalan norak deh. Akhirnya saya dan airen memutuskan untuk ikut jalan jalan! Haha...\r\n\r\nKami pun tiba di ancol jam 11 siang. Biaya masuk kawasan Ancol per-kepala adalah Rp. 15.000, sedangkan untuk mobil dikenakan biaya Rp. 20.000\r\n\r\nSelanjutnya, mobil berputar menuju area  SeaWorld. Tampaknya banyak orang yang memiliki pemikiran sama seperti kami buat rekreasi kemari. Soalnya parkiran lumayan penuh. Masuk ke dalam area seaworld, kami disambut oleh loket pembelian tiket yang terletak di sebelah kiri pintu.  Ada sekitar 5 orang petugas yang stand by. Jam operasional SeaWorld adalah mulai pukul 09.00 sampai 18.00\r\n\r\nHarga tiket masuk SeaWorld adalah sebagai berikut:\r\n\r\nWeekday:\r\nUsia 2 tahun ke atas: Rp. 80.000\r\nLansia (diatas 55 tahun): Rp.48.000\r\n\r\nWeekend dan libur nasional:\r\nUsia 2 tahun ke atas: Rp. 90.000\r\nLansia (diatas 55 tahun): Rp.54.000\r\n\r\nNote: Khusus untuk lansia, bisa mendapatkan harga khusus dengan menunjukkan KTP. Namun, pembayarannya harus dilakukan secara cash.\r\n\r\nKarena gendhis masih di bawah 2 tahun... jadi gratis deh! #Hint: kebetulan ada sepupu saya yang usianya 3 tahun, tapi badannya cukup cilik. Pas masuk, dia di gendong sama mama-nya, jadi gratis juga :p\r\n\r\nDi seberang dari loket terdapat area foodcourt dengan pilihan makanan yang cukup beragam. Tempat duduk yang disediakan juga lumayan banyak. Biar nanti lebih konsen nonton ikannya, karena udah kelaperan kami memutuskan untuk early lunch. Harga makanan yang dijual menurut saya masih masuk di akal. Saya membeli satu paket menu berisi nasi, ayam goreng, union ring, dan lemon tea seharga 25ribu rupiah.\r\n\r\nHmm...I have to say this. Ternyata ya buibu.... Seaworld merupakan tempat rekreasi ideal untuk mengajak bayi bayi dan toddler. Bisa juga jadi tempat rekreasi pertama bagi bayi, selain mall (hehe emangnya gak bosen ke mall meluluuuu?) Alasannya adalah:\r\n\r\nAdem. Ruangannya full AC. Jadi si bayi, toddler, bahkan emak bapaknya nggak akan keringetan. Mereka jadi anteng, nyaman, dan nggak tersiksa selama berekreasi.\r\nStroller friendly. Jalanannya yang rata, tidak berundak undak, dan space-nya yang luas dari satu aquarium ke aquarium lain sungguh sangat bersahabat dengan stroller. Meskipun buat saya pribadi, semenjak punya ergo baby carrier (meskipun kw hehehe) udah hampir nggak pernah bawa stroller kalau berpergian.\r\nMemiliki Nursery room yang cukup terawat. Dindingnya dihiasi lukisan kartun mengenai kehidupan bawah laut seperti ikan dan putri duyung. Terletak di sisi sebelah kiri foodcourt, sejajar dengan toilet. Gendhis sempet dua kali pupu looooh disini. (konon katanya, kalo seseorang bisa pupu di suatu tempat tandanya betah...haha) Wastafelnya bersih dan airnya banyak. \r\nKoleksi ikannya buanyaaaak dengan pengemasan aquarium yang menarik. Seaworld memiliki berbagai koleksi mulai dari biota perairan tawar, terdiri dari 22.000 ekor ikan (126 Jenis), 28 reptil (5 jenis) sampai biota perairan laut yang terdiri dari 5180 ekor ikan (26 jenis), 79 avertebrata (13 jenis), 30 reptil (5 jenis) dan 1 mamalia. Ck..ck...ck... Si anak bakal enjoy banget nih menikmati suguhan ikan berwarna warni yang hilir mudik\r\nLantainya terbuat dari karpet yang empuuuk, terutama di bagian depan aquarium besar sebelum masuk terowongan. Sehingga nggak parno kalo tiba tiba si anak jatoh. Nah ketika udah capek muter muter, tempat ini juga bisa jadi lokasi duduk ngaso yang pewe.\r\nTerdapat ruangan theather yang memutar film tentang satwa laut. #Hint: Psssttt...karena penerangan disini rada temaram dengan bangku bangku kayu panjang, cocok buat jadi spot menyusui. Saya nenenin gendhis sampe dia bobo disini.\r\nSarana edukasi. Buat mulai mengenalkan ke anak seperti apa bentuk penyu, bintang laut, kuda laut, ikan pari, ikan nemo, dll biasanya kan cuman lihat di buku atau nonton TV.\r\nBisa sambil ngasih makan anak. errrr... saya nggak tau deh apakah ada larangan membawa makanan ke dalam area aquarium, tapi buktinya kemarin saya lihat banyak ibu ibu yang ngasih makan anaknya sambil nonton ikan. Jujur, saya sendiri sih nggak sreg ya, karena berpotensi membuat tempat ini jadi kotor sebab banyak makanan berceceran. Tau sendiri doooong kalo anak makan berantakannya kayak apa!!$#@!@$', 'Gendhis Mommy', '2017-02-02 11:34:04'),
(2, 'Liburan ke Travesea', 'Tahun baru kali ini bertepatan di hari Jumat, yang berarti akan menjadi long weekend bagi kita semua. What does it mean? It''s time to have holiday.\r\n\r\nBiasanya, liburan panjang seperti ini ku manfaatkan untuk pergi ke luar kota, traveling, atau ngebolang ke tempat yang jauh. Tapi tidak kali ini. \r\n\r\nLibur tahun baru kemarin ku habiskan di Jakarta. Setelah berunding lama dengan kedua temanku, Juu dan Pund, akhirnya kami memutuskan pergi ke Seaworld Taman Impian Jaya Ancol.\r\n\r\nKami berangkat pukul 13.00 WIB, ketemuan di RS. Mitra Keluarga Kemayoran. Aku yang menginap di kosan Juu pun, berangkat bersama. \r\n\r\nTak lama kemudian Pund pun datang. Kami beriring-iringan menuju Ancol. Jalanan cukup lancar. Kami pun tiba di Ancol dengan selamat. Sampai di pintu masuk Ancol, firasat kami tidak enak. Antrian panjang mengular. Dan benar saja, setelah membayar uang masuk Rp 25.000/orang dan Rp 15.000/motor, kami harus merasakan jalanan yang cukup padat. Macet. Ancol hari itu penuh.\r\nPerlahan, kami pun sampai di Seaworld. Sesampainya disana, kami tak langsung antri tiket. Tapi justru duduk-duduk di rerumputan. Lelah dan letih perjalanan. Dan untuk menghilangkan bete, kami melakukan welfie dengan GoProHero4, hasil pinjaman teman si Juu. Kegiatan ini cukup efektif menghilangkan rasa penat kami.\r\n\r\nTiket masuk hari itu Rp 100.000/orang. Awalnya kami ragu untuk membelinya. Mahal, batin kami. Tapi berhubung sudah sampai sana, kenapa tidak masuk saja. Siapa tahu di dalam sana kami menemukan hal-hal luar biasa. \r\n\r\nTiket sudah kami beli. Kami pun masuk. Sebelumnya, tangan kami diberi stempel sebagai bukti telah menyerahkan tiket ke petugas. Dengan stempel tersebut, kita bisa keluar masuk arena. Misalnya, kita sudah masuk ke dalam, lantas keluar untuk beli makanan/sholat, kita bisa masuk lagi tanpa harus membeli tiket kembali. Cukup tunjukkan tangan kita yang sudah distempel.\r\n\r\nMasuk ke dalam, suasananya ramai. Penuh dengan para orang tua yang mengenalkan anak-anaknya dunia dibawah laut. Berbagai jenis hewan laut diperlihatkan di dalam akuarium raksasa. Kondisi yang sesak dan padat membuat kami kurang bisa menikmati keindahan para penghuni lautan tersebut.\r\n\r\nAkhirnya, setelah selesai keliling, kami pun keluar arena. Kami pun melanjutkan aktivitas welfie kami. Hihihi.\r\n\r\nSekitar pukul 18.30 WIB kami pun pulang dengan hati riang gembira.', 'AnakJalan', '2017-02-02 11:38:21'),
(3, 'Mau Lagi', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.\r\n\r\nAenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus.\r\n\r\nNullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui.\r\n\r\nCras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing.\r\n\r\nPhasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque.\r\n\r\nPhasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci.\r\n\r\nPhasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis', 'AnakKeren', '2017-02-02 16:49:31'),
(4, 'RECOMMENDED!', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia.\r\n\r\nIt is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.\r\n\r\nThe Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.\r\n\r\nWhen she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy.\r\n\r\nThe copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country.\r\n\r\nBut nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their projects again and again. And if she hasn’t been rewritten, then they are still using her.\r\n\r\nFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.\r\n\r\nEven the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline', 'PecintaLaut', '2017-02-02 16:52:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`review_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
