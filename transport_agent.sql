-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2016 at 05:07 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `transport_agent`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_address`
--

CREATE TABLE IF NOT EXISTS `customer_address` (
`id` int(11) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_address`
--

INSERT INTO `customer_address` (`id`, `address`) VALUES
(1, '970 Dwight Court'),
(2, '6389 Waxwing Drive'),
(3, '16 Nevada Lane'),
(4, '2 School Avenue'),
(5, '2743 2nd Drive'),
(6, '842 Chive Plaza'),
(7, '2 Birchwood Parkway'),
(8, '7185 Sachtjen Circle'),
(9, '8 Duke Plaza'),
(10, '1 Ohio Hill'),
(11, '67 Delladonna Court'),
(12, '5 Melrose Crossing'),
(13, '40335 Sloan Lane'),
(14, '4595 Laurel Court'),
(15, '979 Bultman Trail'),
(16, '6 Nancy Road'),
(17, '2 Westridge Street'),
(18, '7381 Maple Lane'),
(19, '20140 Weeping Birch Lane'),
(20, '191 Onsgard Park'),
(21, '8906 Monument Place'),
(22, '66 Warner Parkway'),
(23, '641 Kenwood Trail'),
(24, '82221 Sunfield Lane'),
(25, '217 Fuller Way'),
(26, '544 Riverside Court'),
(27, '978 Sundown Street'),
(28, '610 Old Gate Pass'),
(29, '069 Hagan Place'),
(30, '098 Basil Lane'),
(31, '4 Springview Alley'),
(32, '5120 Basil Lane'),
(33, '0 Crownhardt Avenue'),
(34, '45714 Blue Bill Park Parkway'),
(35, '5488 Sugar Trail'),
(36, '9 Luster Pass'),
(37, '4 Marcy Plaza'),
(38, '0 Forest Run Drive'),
(39, '9832 Merrick Way'),
(40, '8 Stone Corner Avenue'),
(41, '531 Vermont Crossing'),
(42, '01363 Cambridge Center'),
(43, '86 Dawn Circle'),
(44, '2825 Londonderry Pass'),
(45, '8968 Steensland Drive'),
(46, '02088 Amoth Park'),
(47, '4 Packers Park'),
(48, '654 Atwood Parkway'),
(49, '7 Pleasure Place'),
(50, '510 Cottonwood Plaza'),
(51, '8092 Hazelcrest Way'),
(52, '3876 Sunbrook Lane'),
(53, '486 Pearson Circle'),
(54, '11 Grover Point'),
(55, '8 Waywood Court'),
(56, '585 John Wall Place'),
(57, '1843 Hollow Ridge Pass'),
(58, '0694 Mesta Avenue'),
(59, '4682 Spaight Crossing'),
(60, '5813 Veith Crossing'),
(61, '055 Thompson Pass'),
(62, '95740 Elgar Circle'),
(63, '72 Continental Center'),
(64, '37746 Eggendart Crossing'),
(65, '7 Towne Trail'),
(66, '2 Raven Pass'),
(67, '0873 Hauk Lane'),
(68, '2916 Spohn Street'),
(69, '13538 Westend Junction'),
(70, '708 Mayer Lane'),
(71, '048 Onsgard Court'),
(72, '643 Springs Drive'),
(73, '02 Steensland Junction'),
(74, '676 Charing Cross Hill'),
(75, '623 Walton Way'),
(76, '3360 Shelley Street'),
(77, '9 Moland Hill'),
(78, '741 Meadow Vale Pass'),
(79, '24248 Canary Point'),
(80, '873 Bellgrove Terrace'),
(81, '782 Valley Edge Circle'),
(82, '80713 Shopko Plaza'),
(83, '1 Reindahl Parkway'),
(84, '71143 Memorial Point'),
(85, '28 Fair Oaks Crossing'),
(86, '90 Bluestem Point'),
(87, '31394 Thierer Place'),
(88, '9 Kings Avenue'),
(89, '463 Lake View Junction'),
(90, '7 Morrow Lane'),
(91, '5 Sloan Hill'),
(92, '12011 Monument Road'),
(93, '2 Eastlawn Junction'),
(94, '11 Eliot Avenue'),
(95, '372 Village Drive'),
(96, '93 Northridge Point'),
(97, '9702 School Plaza'),
(98, '3970 Gina Center'),
(99, '0 Kipling Alley'),
(100, '35781 Corscot Alley');

-- --------------------------------------------------------

--
-- Table structure for table `customer_contact`
--

CREATE TABLE IF NOT EXISTS `customer_contact` (
`id` int(11) NOT NULL,
  `phone` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_contact`
--

INSERT INTO `customer_contact` (`id`, `phone`) VALUES
(1, '62-(606)200-4631'),
(2, '55-(718)641-0198'),
(3, '86-(976)573-7932'),
(4, '7-(315)371-3101'),
(5, '86-(202)324-6004'),
(6, '51-(128)356-7924'),
(7, '62-(877)676-6678'),
(8, '504-(114)619-4159'),
(9, '62-(357)595-9611'),
(10, '33-(468)268-0989'),
(11, '62-(871)930-0826'),
(12, '62-(399)455-5509'),
(13, '51-(285)361-8356'),
(14, '880-(374)848-8308'),
(15, '62-(139)538-3238'),
(16, '44-(773)676-1237'),
(17, '86-(764)650-5504'),
(18, '86-(398)380-0299'),
(19, '351-(194)816-5283'),
(20, '963-(601)238-8459'),
(21, '86-(150)618-3810'),
(22, '86-(459)135-5749'),
(23, '98-(956)377-2991'),
(24, '55-(252)363-9849'),
(25, '351-(375)798-7960'),
(26, '62-(646)548-9285'),
(27, '57-(146)329-4442'),
(28, '51-(560)958-3675'),
(29, '86-(615)485-2687'),
(30, '507-(803)307-9000'),
(31, '48-(760)415-4898'),
(32, '86-(642)376-6308'),
(33, '223-(274)758-7166'),
(34, '86-(758)334-0848'),
(35, '62-(167)624-6598'),
(36, '86-(543)949-0244'),
(37, '86-(748)758-8811'),
(38, '84-(313)599-9326'),
(39, '86-(610)944-4570'),
(40, '48-(739)166-0516'),
(41, '62-(462)409-5639'),
(42, '86-(993)269-9967'),
(43, '223-(191)676-7500'),
(44, '504-(550)640-9196'),
(45, '62-(528)751-5235'),
(46, '84-(873)572-7493'),
(47, '62-(777)861-9439'),
(48, '51-(465)288-9088'),
(49, '62-(340)654-0059'),
(50, '7-(951)265-6618'),
(51, '86-(762)545-4236'),
(52, '95-(331)696-6427'),
(53, '254-(599)240-8635'),
(54, '358-(356)883-5451'),
(55, '976-(613)996-2693'),
(56, '995-(927)415-2890'),
(57, '251-(166)676-9299'),
(58, '591-(524)838-0846'),
(59, '86-(416)345-7218'),
(60, '55-(367)426-8258'),
(61, '86-(138)686-3535'),
(62, '1-(847)377-9191'),
(63, '381-(952)313-4265'),
(64, '62-(783)815-4580'),
(65, '62-(427)729-3064'),
(66, '420-(543)415-3753'),
(67, '1-(757)101-6865'),
(68, '86-(721)509-1017'),
(69, '995-(700)830-4861'),
(70, '1-(549)479-2660'),
(71, '46-(262)293-1336'),
(72, '86-(903)626-6429'),
(73, '503-(958)633-2847'),
(74, '57-(818)288-8133'),
(75, '7-(490)325-1355'),
(76, '62-(285)164-9551'),
(77, '81-(662)569-6445'),
(78, '30-(696)306-2829'),
(79, '234-(156)322-7346'),
(80, '92-(813)961-6672'),
(81, '55-(732)855-8905'),
(82, '86-(721)592-9065'),
(83, '63-(335)932-5480'),
(84, '86-(622)816-3220'),
(85, '86-(702)297-6288'),
(86, '31-(516)160-1092'),
(87, '234-(390)349-5015'),
(88, '351-(209)642-7150'),
(89, '504-(747)483-0878'),
(90, '7-(852)869-1633'),
(91, '63-(105)311-7930'),
(92, '86-(304)466-5874'),
(93, '55-(338)489-0823'),
(94, '374-(144)116-8654'),
(95, '509-(254)574-7598'),
(96, '86-(731)895-1540'),
(97, '86-(779)777-1999'),
(98, '86-(888)202-9045'),
(99, '30-(352)234-7154'),
(100, '48-(566)698-8102');

-- --------------------------------------------------------

--
-- Table structure for table `customer_name`
--

CREATE TABLE IF NOT EXISTS `customer_name` (
`id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_name`
--

INSERT INTO `customer_name` (`id`, `first_name`, `last_name`) VALUES
(1, 'Carlos', 'Lèi'),
(2, 'Barbara', 'Réservés'),
(3, 'Brenda', 'Annotée'),
(4, 'Julie', 'Cléopatre'),
(5, 'Rebecca', 'Véronique'),
(6, 'Benjamin', 'Marie-ève'),
(7, 'Robert', 'Clémentine'),
(8, 'Patricia', 'Nélie'),
(9, 'Debra', 'Renée'),
(10, 'Emily', 'Personnalisée'),
(11, 'Howard', 'Alizée'),
(12, 'Kathleen', 'Maïté'),
(13, 'Douglas', 'Lèi'),
(14, 'Katherine', 'Jú'),
(15, 'Nicholas', 'Océanne'),
(16, 'Christopher', 'Hélèna'),
(17, 'Pamela', 'Aloïs'),
(18, 'Laura', 'Méng'),
(19, 'Joyce', 'Lèi'),
(20, 'Aaron', 'Personnalisée'),
(21, 'Ronald', 'Mélissandre'),
(22, 'Billy', 'Miléna'),
(23, 'Betty', 'Nélie'),
(24, 'Philip', 'Aí'),
(25, 'James', 'Liè'),
(26, 'Maria', 'Marylène'),
(27, 'Martha', 'Personnalisée'),
(28, 'Craig', 'Adélie'),
(29, 'Steven', 'Chloé'),
(30, 'Tina', 'Tán'),
(31, 'Ashley', 'Crééz'),
(32, 'Gary', 'Faîtes'),
(33, 'Helen', 'Léa'),
(34, 'Irene', 'Jú'),
(35, 'Steven', 'Irène'),
(36, 'Donna', 'Yú'),
(37, 'Ryan', 'Personnalisée'),
(38, 'Stephanie', 'Aurélie'),
(39, 'William', 'Gaïa'),
(40, 'Sean', 'Océanne'),
(41, 'Louis', 'Eliès'),
(42, 'Louise', 'Garçon'),
(43, 'Linda', 'Dù'),
(44, 'Catherine', 'Almérinda'),
(45, 'Dorothy', 'Agnès'),
(46, 'Nicholas', 'Torbjörn'),
(47, 'Mary', 'Maïwenn'),
(48, 'Stephanie', 'Célia'),
(49, 'Jonathan', 'Océanne'),
(50, 'Shawn', 'Eléa'),
(51, 'Margaret', 'Naéva'),
(52, 'Donna', 'Léandre'),
(53, 'Annie', 'Nadège'),
(54, 'Jonathan', 'Méryl'),
(55, 'Gary', 'Gwenaëlle'),
(56, 'Susan', 'Mélia'),
(57, 'Samuel', 'Maïwenn'),
(58, 'Timothy', 'Aurélie'),
(59, 'Mark', 'Mahélie'),
(60, 'Theresa', 'Märta'),
(61, 'Sean', 'Thérèse'),
(62, 'Judith', 'Märta'),
(63, 'Timothy', 'Rachèle'),
(64, 'Wayne', 'Åke'),
(65, 'Lillian', 'Mén'),
(66, 'Chris', 'Erwéi'),
(67, 'Carlos', 'Estève'),
(68, 'Robin', 'Åslög'),
(69, 'Mark', 'Marie-hélène'),
(70, 'Irene', 'Styrbjörn'),
(71, 'Amy', 'Zoé'),
(72, 'Larry', 'Marie-françoise'),
(73, 'Stephanie', 'Geneviève'),
(74, 'Rachel', 'Zoé'),
(75, 'Dorothy', 'Mylène'),
(76, 'Eugene', 'Célia'),
(77, 'Amy', 'Personnalisée'),
(78, 'Charles', 'Mà'),
(79, 'Heather', 'Audréanne'),
(80, 'Carolyn', 'Séréna'),
(81, 'Jose', 'Gwenaëlle'),
(82, 'Bruce', 'Personnalisée'),
(83, 'Donald', 'Ruò'),
(84, 'Beverly', 'Yú'),
(85, 'Diane', 'Géraldine'),
(86, 'Chris', 'Bérangère'),
(87, 'Robin', 'Crééz'),
(88, 'Gary', 'Loïc'),
(89, 'Christopher', 'Thérèsa'),
(90, 'Mark', 'Célestine'),
(91, 'Lawrence', 'Thérèsa'),
(92, 'Alice', 'Anaël'),
(93, 'Jane', 'Edmée'),
(94, 'Ernest', 'Néhémie'),
(95, 'Henry', 'Maëlys'),
(96, 'Judy', 'Léana'),
(97, 'Diana', 'Yú'),
(98, 'Theresa', 'Edmée'),
(99, 'Donald', 'Adélie'),
(100, 'Kimberly', 'Aí');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_address`
--
ALTER TABLE `customer_address`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_contact`
--
ALTER TABLE `customer_contact`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_name`
--
ALTER TABLE `customer_name`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_address`
--
ALTER TABLE `customer_address`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `customer_contact`
--
ALTER TABLE `customer_contact`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `customer_name`
--
ALTER TABLE `customer_name`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
