<!-- Reference: contoh_codeigniter di scele -->
<?php

class Customer_name_model extends CI_Model {
	private $table_name = 'customer_name';

	function  __construct(){
		parent::__construct(); 
	}

	function fetch_customer_name(){
		$query = $this->db->get($this->table_name);
		if($query->num_rows() > 0) return $query->result();
	}
}