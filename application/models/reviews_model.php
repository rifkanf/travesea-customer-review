<!-- Reference: contoh_codeigniter di scele -->
<?php

class Reviews_model extends CI_Model {
	private $table_name = 'reviews';

	function  __construct(){
		parent::__construct(); 
	}

	function fetch_reviews(){
		$query = $this->db->get($this->table_name);
		if($query->num_rows() > 0) return $query->result();
	}

	function fetch_review($id){
		$query = $this->db->get_where($this->table_name, array('review_id' => $id));
		if($query->num_rows() > 0) {
			return $query->result();	
		} else {
			header("Location: ". base_url(). "index.php/customer-reviews");
		}
	}

	function add_new_review($nama, $judul, $konten){
		$data = array(
			'author' => $nama,
			'title' => $judul,
			'content' => $konten,
		);

		$this->db->insert('reviews',$data);
	}

	function add_comment($review_id, $nama, $komen){
		$data = array(
			'review_id' => $review_id,
			'commenter' => $nama,
			'comment' => $komen,
		);

		$this->db->insert('comments',$data);
	}
}