<!-- Reference: contoh_codeigniter di scele -->
<?php

class Customer_address_model extends CI_Model {
	private $table_name = 'customer_address';

	function  __construct(){
		parent::__construct(); 
	}

	function fetch_customer_address(){
		$query = $this->db->get($this->table_name);
		if($query->num_rows() > 0) return $query->result();
	}
}