<?php
	if(isset($_GET["postbtn"])) {
		$nama = $_GET["nama"];
		$judul = $_GET["judul"];
		$konten = $_GET["konten"];

		header("Location: " .base_url() .'index.php/CustomerReviews/add_review?nama='.urlencode($nama).'&judul='.urlencode($judul).'&konten='.urlencode($konten).'');
	}
?>
<div class="container" style="padding-top: 70px; padding-bottom: 50px; text-align: center;">
	<p style="font-size: 50px; text-align: center;">Write Your Own Review</p>

	<form method="GET" action="<?php echo base_url(); ?>index.php/CustomerReviews/write_review" style="padding-top: 20px">
		<div class="form-group">
			<input style="font-size: 22px" class="form-control" type="text" placeholder="Nama" id="nama" name="nama" >
		</div>
		<div class="form-group">
			<input style="font-size: 22px" class="form-control" type="text" placeholder="Judul" id="judul" name="judul">
		</div>
		<div class="form-group">
			<textarea style="font-size: 22px" class="form-control" placeholder="Konten" rows="15" id="konten" name="konten"></textarea>
		</div>
		<a href=""><button style="float: right; font-size: 25px" class='btn btn-primary' id="postbtn" name="postbtn">Post Review</button></a>
	</form>
</div>