<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title> <?=$title?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href='//fonts.googleapis.com/css?family=Chewy' rel='stylesheet'>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/dolphin.png">
  <a name="top"></a>
</head>
<body>
    <a name="top"></a>
    <nav class="navbar navbar-trans navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>" id="brand">Travesea</a>
        </div>
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/images/dolphin.png" width='30px'>
            </a>
        </div>
      </div>
    </nav>