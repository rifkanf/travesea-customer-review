	<p id="title"><?php echo $title; ?></p>
	<div class="container main">
		<table class="table table-hover">
	      <tr>
	        <th>No.</th>
	        <th>Nama Pelanggan</th>
	        <th>Alamat</th>
	        <th>Phone Number</th>
	      </tr>

	      <?php
	      	$db_size = count($customer_name_results);
	        for($i = 0; $i < $db_size; $i++) { ?>
	          <tr>
	            <td><?= $customer_name_results[$i]->id;?></td>
	            <td><?= $customer_name_results[$i]->first_name;?> <?= $customer_name_results[$i]->last_name;?></td>
	            <td><?= $customer_address_results[$i]->address;?></td>
	            <td><?= $customer_contact_results[$i]->phone;?></td>
	          </tr>
	      <?php  }
	      ?>
	    </table>
	</div>