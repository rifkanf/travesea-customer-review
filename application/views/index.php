    <!-- Rerference for carousel: w3school -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active slide2">
          <p class="juduljumbo">AMAZING SEA VIEW</p>
          <p class="kontenjumbo">Satisfy your eyes with alluring sea view featuring so many fishes and other sea creatures!</p>
        </div>
        <div class="item slide1">
          <p class="juduljumbo">TRIP TO THE SEA</p>
          <p class="kontenjumbo">Get the chance to trip with the world's most amazing trip to the sea holder by Travesea!</p>
        </div>
        <div class="item slide5">
          <p class="juduljumbo">INTRIGUING DIVING</p>
          <p class="kontenjumbo">Feel the most breathtaking feeling in the sea by joining diving package by Travesea!</p>
        </div>
      </div>

      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="container-fluid konten" id="content">
        <div class="container">
            <div class="row chooseus slideanim">
                <p class="judulkonten">What We Offer<img src="<?php echo base_url(); ?>assets/images/bubbles.png" width='100px'></p>
                <div class="col-sm-4">
                    <img src="<?php echo base_url(); ?>assets/images/fish.png" width="200px" height="200px">
                    <h1>SEA VIEW</h1>
                    <p class="isikonten">Get the experience to see an alluring sea view with so many fishes!</p>
                </div>
                <div class="col-sm-4">
                    <img src="<?php echo base_url(); ?>assets/images/diving.png" width="200px" height="200px">
                    <h1>DIVING</h1>
                    <p class="isikonten">Feel an impressive time in the sea with our diving package!</p>
                </div>
                <div class="col-sm-4">
                    <img src="<?php echo base_url(); ?>assets/images/boat.png" width="200px" height="200px">
                    <h1>TRIP</h1>
                    <p class="isikonten">Don't miss the world's most amazing trip to the sea by Travesea!</p>
                </div>
            </div>
        </div>
    </div>




    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" id="customer">
      <div class="container slideanim">
        <h1>Customer Reviews <img src="<?php echo base_url(); ?>assets/images/people.png" width='100px'></h1>
        <p id="isicustomer"><?= $content ?></p>
        <p><a class="btn btn-info btn-lg" href="<?php echo base_url(); ?>index.php/customer-reviews" role="button">See Reviews &raquo;</a></p>
      </div>
    </div>

    <div class="container maps" id="maps">
      <div class="row slideanim">
        <div class="col-sm-5">
          <div id="googleMap" class="container" style="width: 500px; height: 380px;"></div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-5">
          <p class="judulkonten" id="route">Route to Travesea!</p>
          <p id="desc">Confused to get to our place?<br>Use this map to find your route!</p>
          <div class="col-xs-4 map-cont">
            <p>Travel Mode:</p>
          </div>
          <div class="form-group col-xs-4" id="floating-panel">
              <select class="form-control" id="mode">
                <option value="DRIVING">Driving</option>
                <option value="WALKING">Walking</option>
                <option value="TRANSIT">Transit</option>
              </select>
          </div>
        </div>
      </div>
    </div>