<?php
	session_start();
	unset($_SESSION["review_id"]);
	$_SESSION["review_id"] = $review[0]->review_id;

	if(isset($_GET["commentbtn"])) {
		$nama = $_GET["nama"];
		$komen = $_GET["komen"];

		header("Location: " .base_url() .'index.php/CustomerReviews/add_comment?nama='.urlencode($nama).'&review_id='.urlencode($_SESSION["review_id"]).'&komen='.urlencode($komen).'');
	}
?>
<div class="container">
	<p style="font-size: 50px; padding-top: 70px; text-align: center;"><?= $review[0]->title ?></p>
	<p style="font-size: 18px; text-align: center;">published by <?= $review[0]->author ?> on <?= $review[0]->time ?></p>

	<p style="font-size: 20px; text-align: justify; padding-top: 50px"><?= $review[0]->content ?></p>
	<p style="font-size: 20px; float: left"><a style="text-decoration: none" href="<?php echo base_url(); ?>index.php/CustomerReviews">&lt;&lt; Back to Reviews</a></p>

	<div class="row" style="padding-top: 70px">
		<div class="col-sm-6">
			<form method='GET' action="<?php echo base_url(); ?>index.php/CustomerReviews/review/<?php echo $_SESSION["review_id"] ?>" id="comment-form">
				<p style="font-size: 30px">Post your comment:</p>
				<div class="form-group">
					<input type="text" class="form-control" id="name" name="nama" placeholder="Nama" style="font-size: 17px">
				</div>
				<div class="form-group">
					<textarea class="form-control" rows="5" id="komen" name="komen" placeholder="Your comment..." style="font-size: 17px"></textarea>
				</div>
				<button style="float: right; font-size: 18px" class='btn btn-primary' id="commentbtn" name="commentbtn">Submit</button>
			</form>
		</div>
	</div>
	<p style='font-size: 30px;'>Comment(s)</p>
	<div id="comments"></div>
</div>