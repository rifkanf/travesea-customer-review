<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title> <?=$title?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href='//fonts.googleapis.com/css?family=Chewy' rel='stylesheet'>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/dolphin.png">
  <a name="top"></a>
</head>
<body>
    <a name="top"></a>
    <nav class="navbar navbar-trans navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>" id="brand">Travesea</a>
        </div>
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/images/dolphin.png" width='30px'>
            </a>
        </div>
        <div class="navbar-collapse collapse" id="navbar-collapsible">
          <ul class="nav navbar-nav navbar-right">
               <li><a href="#content">What We Offer</a></li>
                <li><a href="#customer">Reviews</a></li>
                <li><a href="#maps">Maps</a></li>
                <li>
                    <a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-search"></span></a>
                </li>
            </ul>
        </div>
      </div>
    </nav>