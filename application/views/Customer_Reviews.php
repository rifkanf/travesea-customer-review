<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<p style="font-size: 50px; padding-top: 70px; text-align: center;">Customer Reviews</p>
			<?php
		      	$db_size = count($reviews);
		        for($i = $db_size-1; $i >= 0; $i--) { ?>
		        	<p style="font-size: 30px; padding-top: 30px"><a href="<?php echo base_url(); ?>index.php/CustomerReviews/review/<?php echo $reviews[$i]->review_id ?>" style="text-decoration: none"><?= $reviews[$i]->title;?></a></p>
		        	<p style="font-size: 17px; color: #002f7c">published by <?= $reviews[$i]->author;?> on <?= $reviews[$i]->time;?></p>
		          	<?php
		          		if(strlen($reviews[$i]->content) > 500) { ?>
		          			<p style="text-align: justify; font-size: 18px"><?= substr($reviews[$i]->content, 0, 500); ?>...</p>
		          		<?php } else {	?>
		          			<p style="text-align: justify; font-size: 18px"><?= $reviews[$i]->content; ?></p>
		          		<?php }
		          		?>
		          	<p style="float: right; font-size: 17px; padding-bottom: 20px"><a href="<?php echo base_url(); ?>index.php/CustomerReviews/review/<?php echo $reviews[$i]->review_id ?>" style="text-decoration: none">Read more</a></p>
		      <?php  }
		      ?>
		</div>
		<div class="col-sm-4">
			<p style="font-size: 50px; padding-top: 150px; text-align: center;">It's time to write your own review!</p>
			<p style="text-align: center;"><a class="btn btn-danger btn-lg" href="<?php echo base_url(); ?>index.php/CustomerReviews/write_review" role="button">Write Review &raquo;</a></p>
		</div>
	</div>
</div>