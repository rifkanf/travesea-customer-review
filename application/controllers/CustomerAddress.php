<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerAddress extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Customer_name_model');
		$this->load->model('Customer_address_model');
		$this->load->model('Customer_contact_model');
	}

	public function index()
	{	
		$data['title'] = "List of Customers";
		$data['customer_name_results'] = $this->Customer_name_model->fetch_customer_name();
		$data['customer_address_results'] = $this->Customer_address_model->fetch_customer_address();
		$data['customer_contact_results'] = $this->Customer_contact_model->fetch_customer_contact();

		$this->load->view("header", $data);
		$this->load->view('Customer_Address', $data);
		$this->load->view("footer", $data);
	}
}
