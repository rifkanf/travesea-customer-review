<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerReviews extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Reviews_model');
	}

	public function index()
	{	
		$data['title'] = "Travesea | Customer Reviews";
		$data['reviews'] = $this->Reviews_model->fetch_reviews();

		$this->load->view("header_review", $data);
		$this->load->view('Customer_Reviews', $data);
		$this->load->view("footer", $data);
	}

	public function review($id)
	{
		$data['title'] = "Travesea | Review";
		$data['review'] = $this->Reviews_model->fetch_review($id);

		$this->load->view("header_review", $data);
		$this->load->view('Review_detail', $data);
		$this->load->view("footer", $data);
	}

	public function write_review()
	{
		$data['title'] = "Travesea | Write Review";

		$this->load->view("header_review", $data);
		$this->load->view('Write_review', $data);
		$this->load->view("footer", $data);
	}

	public function add_review()
	{
		if($this->input->get()) {
			$nama = $this->input->get('nama');
			$judul = $this->input->get('judul');
			$konten = $this->input->get('konten');

			$this->Reviews_model->add_new_review($nama, $judul, $konten);

			header("Location: ".base_url() ."index.php/CustomerReviews");
		}
	}

	public function add_comment()
	{
		if($this->input->get()) {
			$review_id = $this->input->get('review_id');
			$nama = $this->input->get('nama');
			$komen = $this->input->get('komen');

			$this->Reviews_model->add_comment($review_id, $nama, $komen);

			header("Location: ".base_url() ."index.php/CustomerReviews/review/" .$review_id ."#comments");
		}
	}
}