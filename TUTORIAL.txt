I made a web page not only could make and review posts but also could leave comments at each post.

1. Run the Apache and MySQL server (you can use xampp).
2. Make a new database in phpmyadmin with this name: "customer_reviews".
3. Import customer_reviews.sql file to the database whom you just made at point 2.
4. Save "travesea" folder to localhost apache server (htdocs in xampp).
5. Run any browser and write "localhost/travesea-customer-review" on the browser address.
6. To see the review page that I told before you could click "Review" at the navigation (on top-right web page).
7. Click "See Review" button that will open a web page filled with all the review posts.
8. Each title and the word "read more" at each review post could be clicked to see a complete review.
9. To add more review post, you should click "Write review" button which is placed at the right of web page that displays all the reviews before (page on point 7).
10. You could fill the form which is placed at the bottom of each post to leave a comment.